import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MidlevelpageComponent } from './midlevelpage.component';

describe('MidlevelpageComponent', () => {
  let component: MidlevelpageComponent;
  let fixture: ComponentFixture<MidlevelpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MidlevelpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MidlevelpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
