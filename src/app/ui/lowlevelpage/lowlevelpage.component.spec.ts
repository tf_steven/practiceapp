import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LowlevelpageComponent } from './lowlevelpage.component';

describe('LowlevelpageComponent', () => {
  let component: LowlevelpageComponent;
  let fixture: ComponentFixture<LowlevelpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LowlevelpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LowlevelpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
