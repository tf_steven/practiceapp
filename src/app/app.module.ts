import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

///// Start FireStarter

// Core
import { CoreModule } from './core/core.module';

// Shared/Widget
import { SharedModule } from './shared/shared.module';

// Feature Modules
// import { UploadModule } from './uploads/shared/upload.module';
import { UiModule } from './ui/shared/ui.module';
///// End FireStarter

import { environment } from '../environments/environment';

import { AngularFireModule } from 'angularfire2';
export const firebaseConfig = environment.firebaseConfig;
// import { AngularFirestoreModule } from 'angularfire2/firestore';
import {LowlevelpageComponent} from './ui/lowlevelpage/lowlevelpage.component';
import {MidlevelpageComponent} from './ui/midlevelpage/midlevelpage.component';
import {AdminpageComponent} from './ui/adminpage/adminpage.component';
import {MaterialModule} from './material.module';
import {NewsfeedComponent} from './newsfeed/newsfeed.component';
import {NewsService} from './news.service';
import {HttpModule} from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    LowlevelpageComponent,
    MidlevelpageComponent,
    AdminpageComponent,
    NewsfeedComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    CoreModule,
    SharedModule,
    UiModule,
    AngularFireModule.initializeApp(firebaseConfig),
    MaterialModule,
    HttpModule,
  ],
  bootstrap: [
    AppComponent,
  ],
  providers: [NewsService],
})
export class AppModule { }
