
import {AfterViewInit, Component, OnInit} from '@angular/core';
import {NewsService} from '../news.service';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-newsfeed',
  templateUrl: './newsfeed.component.html',
  styleUrls: ['./newsfeed.component.css'],
})
export class NewsfeedComponent implements AfterViewInit, OnInit {
  title;
  newsfeed;
  images;
  search;
  drop = false;
  li;
  timeup = false;
  searchTitle = 'EUROPE';
  customSearch = false;
  europe;
  uae;
  asia;
  africa;
  premierleauge;
  music;
  movies;
  currency;
  fashion;
  provider;
  newscollection = [];
  searcheurope = 'europe';

  newsarray: any[] = ['europe', 'uae', 'asia', 'africa', 'currency', 'fashion', 'music', 'music', 'movies', 'premierleauge'];
  stringarray: any[] = ['europe', 'uae', 'asia', 'africa', 'currency', 'fashion and design', 'music', 'movies', 'premier league'];

  // categories: any[] = [
  //   {
  //     name: 'europe', 'uae', 'asia', 'africa', 'currency', 'fashion and design', 'music', 'movies', 'premier league',
  //     slug: 'europe', 'uae', 'asia', 'africa', 'currency', 'fashion', 'music', 'music', 'movies', 'premierleauge',
  //   },
  // ];

  constructor(private svc: NewsService) {
  }

  // public changeTab(tabgroup) {
  //   const pid = tabgroup._currentLabelWrapper.id;
  //   const activeTab = ('#' + pid).text().trim();
  //   console.dir('Active tab: ' + activeTab);
  // }

  collection() {
  }

  // setTimeout( );

// load by search
//  loadSearch() {
//    this.searchTitle = this.search.toUpperCase();
//    this.europe = this.svc.getfee(this.search)
//      .subscribe(data => {
//        const feed1 = data[0].articles;
//        const feed2 = data[1].articles;
//
//        this.europe = feed2;
//        this.customSearch = true;
//        console.log(this.newsfeed);
//      });
//  }

  latesNews() {
// this.svc.getLatest();
  }

  // show drop down
  // showDropdown() {
  //   this.drop = true;
  // }

  // closeDropdown() {
  //   this.search = this.li;
  //   this.drop = false;
  // }

  ngOnInit() {
    this.title = 'Results';
    // get feed

    const searcheurope = 'europe';
    const searchuae = 'uae';
    const searchafrica = 'africa';
    const searchasia = 'asia';
    const searchmovies = 'movies';
    const searchmusic = 'music';
    const searchpremierleauge = 'premier league';
    const searchcurrency = 'currency';
    const searchfashion = 'fashion and design';
    const newsarray = ['europe', 'uae', 'asia', 'africa', 'currency', 'fashion', 'music', 'music', 'movies', 'premierleauge'];
    const stringarray = ['europe', 'uae', 'asia', 'africa', 'currency', 'fashion and design', 'music', 'movies', 'premier league'];

// create call functions for this, DRY
//     // EuropeNewsPull () {}
//     this.europe = this.svc.getfee(searcheurope)
//       .subscribe(data => {
//         const feed1 = data[0].articles;
//         const feed2 = data[1].articles;
//
//         this.europe = feed2;
//
//         console.log(this.europe);  // this function is pulling 20 request and consistent across the other request, urgent fix, !todo
//       });                          // possible band-aid fix, add counter and exit after x amount of pulls
//                                   // SHOULD NOT BE ngOnInit, MOVE!
//     UaeNewsPull ()
//     {
//       this.uae = this.svc.getfee(searchuae)
//         .subscribe(data => {
//           const feed1 = data[0].articles;
//           const feed2 = data[1].articles;
//
//           this.uae = feed2;
//
//           console.log(this.uae);
//         });
//     }
    //
    // AsiaNewsPull ()
    // {
    //   this.asia = this.svc.getfee(searchasia)
    //     .subscribe(data => {
    //       const feed1 = data[0].articles;
    //       const feed2 = data[1].articles;
    //
    //       this.asia = feed2;
    //
    //       console.log(this.asia);
    //     });
    // }
    //
    // AfricaNewsPull () {}
    // this.africa = this.svc.getfee(searchafrica)
    //   .subscribe(data => {
    //     const feed1 = data[0].articles;
    //     const feed2 = data[1].articles;
    //
    //     this.africa = feed2;
    //
    //     console.log(this.africa);
    //   });
    // SportsNewsPull () {}
    // this.premierleauge = this.svc.getfee(searchpremierleauge)
    //   .subscribe(data => {
    //     const feed1 = data[0].articles;
    //     const feed2 = data[1].articles;
    //
    //     this.premierleauge = feed2;
    //
    //     console.log(this.premierleauge);
    //   });
    // MoviesNewsPull () {}
    // this.movies = this.svc.getfee(searchmovies)
    //   .subscribe(data => {
    //     const feed1 = data[0].articles;
    //     const feed2 = data[1].articles;
    //
    //     this.movies = feed2;
    //
    //     console.log(this.movies);
    //   });
    //
    //
    // MusicNewsPull () {}
    // this.music = this.svc.getfee(searchmusic)
    //   .subscribe(data => {
    //     const feed1 = data[0].articles;
    //     const feed2 = data[1].articles;
    //
    //     this.music = feed2;
    //
    //     console.log(this.movies);
    //   });
    // CurrencyNewsPull () {}
    // this.currency = this.svc.getfee(searchcurrency)
    //   .subscribe(data => {
    //     const feed1 = data[0].articles;
    //     const feed2 = data[1].articles;
    //
    //     this.currency = feed2;
    //
    //     console.log(this.currency
    //     );
    //   });
    //
    // FashionNewsPull () {}
    // this.fashion = this.svc.getfee(searchfashion)
    //   .subscribe(data => {
    //     const feed1 = data[0].articles;
    //     const feed2 = data[1].articles;
    //
    //     this.fashion = feed2;
    //
    //     console.log(this.fashion
    //     );
    //   });

    this.collection();

  }

  ngAfterViewInit() {
    // console.log('afterViewInit => ', this.selectedIndex);
  }
// #europe tabGroup (europeTabChange)="tabChanged($event)
  EuropeNewsPull() {
    this.europe = this.svc.getfee(this.stringarray[0])
      .subscribe((data) => {
        console.log(this.searcheurope);
        console.log(this.searchuae);
        const feed1 = data[0].articles;
        const feed2 = data[1].articles;

        this.europe = feed2;

        console.log(this.newsarray[0]);  // this function is pulling 20 request and consistent across the other request, urgent fix, !todo
      });
  }
  UaeNewsPull () {
    this.newsarray[0] = this.svc.getfee(this.stringarray[0])
      .subscribe((data) => {
        const feed1 = data[0].articles;
        const feed2 = data[1].articles;

        this.newsarray[0] = feed2;

        console.log(this.newsarray[0]);
      });
  }

  AsiaNewsPull () {
    this.asia = this.svc.getfee(this.searchasia)
      .subscribe((data) => {
        const feed1 = data[0].articles;
        const feed2 = data[1].articles;

        this.asia = feed2;

        console.log(this.asia);
      });
  }
// #tabGroup (selectedTabChange)="tabChanged($event)
//   tabChanged = (tabChangeEvent): void => {
//     console.log('tabChangeEvent => ', tabChangeEvent);
//     console.log('index => ', tabChangeEvent.index);
//     console.log(this.europe);
//     console.log(this.stringarray[[tabChangeEvent.index]]);
//     console.log(this.searcheurope);
//     console.log(this.newsarray[[tabChangeEvent.index]]);
    // this.europe = this.svc.getfee(this.searcheurope)
    //   .subscribe((data) => {
    //     const feed1 = data[0].articles;
    //     const feed2 = data[1].articles;
    //
    //     this.europe = feed2;
    //
    //     console.log(this.countrysearch[tabChangeEvent.index]);
    // this.newsarray[[tabChangeEvent.index]] = this.svc.getfee(this.stringarray[[tabChangeEvent.index]])
      // .subscribe((data) => {
      //   const feed1 = data[0].articles;
      //   const feed2 = data[1].articles;
      //   console.log('completed');
      //
      //   this.newsarray[[tabChangeEvent.index]] = feed2;
        // console.log(this.stringsearch);
  //     });  // Function works and is dynamic... Now it just needs to actually be called with tabs
  // }
  onSelectChange(event) {
    console.log('tabChangeEvent => ', event);
    console.log('index => ', event.index);
    console.log(this.stringarray[event.index]);
    // console.log(this.searcheurope);
    console.log(this.newsarray[event.index]);
    // this.europe = this.svc.getfee(this.searcheurope)
    //   .subscribe((data) => {
    //     const feed1 = data[0].articles;
    //     const feed2 = data[1].articles;
    //
    //     this.europe = feed2;
    //
    //     console.log(this.countrysearch[tabChangeEvent.index]);
    this.newsarray[event.index] = this.svc.getfee(this.stringarray[event.index])    // !todo, bind the data dynamically to this array.
      .subscribe((data) => {
        const feed1 = data[0].articles;
        const feed2 = data[1].articles;
        console.log('completed');

        this.newsarray[event.index] = feed1;
        this.newsarray[event.index] = feed2;
        // console.log(this.stringsearch);
      });  // Function works and is dynamic... Need to bind the data to the tabs/cards to display it. Clean up data structure after.
  }
}
  //   function NewsPull (searchedcountry, searchedstring) {
  //     this.searchedcountry = this.svc.getfee(searchedstring)
  //       .subscribe(data = {
  //         const feed1 = data[0].articles;
  //     const feed2 = data[1].articles;
  //
  //     this.searchedcountry = feed2;
  //
  //     console.log(this.searchedcountry),
  //   console.log(this.stringsearch)
  //   });}
  // }

  // each tab has its own index number
  // should be possible to call a NewsPull function with each different tab
  // pass in parameters consistent with what tab is being called
  // ex Foreign News tab = index 1 so pass in foreignnews into the NewsPull function
  // tab.ChangeEvent.index
  // const newsarray = [ europe, uae, asia, africa, currency, fashion, music, music, movies, premierleauge ]
  // const stringarray = [ europe, uae, asia, africa, currency, fashion and design, music, movies, premier league]
  // searchedcountry = newsarray[tabChangeEvent.index]
  // searchedstring = stringarray[tabChangeEvent.index]

  // function;

//   NewsPull() {
//     this.countrysearch[tabChangeEvent.index] = this.svc.getfee(this.searchstring(tabChangeEvent.index))
//       .subscribe((data) => {
//         const feed1 = data[0].articles;
//         const feed2 = data[1].articles;
//         console.log(this.searchedcountry);
//         console.log(this.stringsearch);
//       });
//   }
// // }

    // const searcheurope = 'europe';
    // const searchuae = 'uae';
    // const searchafrica = 'africa';     strings being passed into this.getfee(searchedstring)
    // const searchasia = 'asia';
    // const searchmovies = 'movies';
    // const searchmusic = 'music';
    // const searchpremierleauge = 'premier league';
    // const searchcurrency = 'currency';
    // const searchfashion = 'fashion and design'
