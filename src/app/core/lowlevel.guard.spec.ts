import { TestBed, async, inject } from '@angular/core/testing';

import { LowlevelGuard } from './lowlevel.guard';

describe('LowlevelGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LowlevelGuard]
    });
  });

  it('should ...', inject([LowlevelGuard], (guard: LowlevelGuard) => {
    expect(guard).toBeTruthy();
  }));
});
