import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import * as _ from 'lodash';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean {

    return this.auth.user
      .take(1)
      .map((user) => _.has(_.get(user, 'roles'), 'admin'))
      .do((authorized) => {
        if (!authorized) {
          alert('Only admins may access this page! Login with admin@admin.com with the password admin1 to try the admin routing');
          this.router.navigate(['/login']);
        }
      });

  }
}
