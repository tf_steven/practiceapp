import { TestBed, async, inject } from '@angular/core/testing';

import { MidlevelGuard } from './midlevel.guard';

describe('MidlevelGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MidlevelGuard]
    });
  });

  it('should ...', inject([MidlevelGuard], (guard: MidlevelGuard) => {
    expect(guard).toBeTruthy();
  }));
});
