import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserLoginComponent } from './ui/user-login/user-login.component';
import { ReadmePageComponent } from './ui/readme-page/readme-page.component';

import { AuthGuard } from './core/auth.guard';
import { CoreModule } from './core/core.module';

import {AdminpageComponent} from './ui/adminpage/adminpage.component';
import {AdminGuard} from './core/admin.guard';
import {MidlevelpageComponent} from './ui/midlevelpage/midlevelpage.component';
import {LowlevelGuard} from './core/lowlevel.guard';
import {MidlevelGuard} from './core/midlevel.guard';
import {LowlevelpageComponent} from './ui/lowlevelpage/lowlevelpage.component';
import {NewsfeedComponent} from './newsfeed/newsfeed.component';

const routes: Routes = [
  { path: '', component: ReadmePageComponent },
  { path: 'login', component: UserLoginComponent },
  { path: 'news', component: NewsfeedComponent },
  { path: 'uploads', loadChildren: './uploads/shared/upload.module#UploadModule', canActivate: [AuthGuard] },
  { path: 'lowlevel', component: LowlevelpageComponent, canActivate: [AuthGuard, LowlevelGuard]},
  { path: 'midlevel', component: MidlevelpageComponent, canActivate: [MidlevelGuard]},
  { path: 'admin', component: AdminpageComponent, canActivate: [AdminGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard, LowlevelGuard, MidlevelGuard, AdminGuard],
})
export class AppRoutingModule { }
